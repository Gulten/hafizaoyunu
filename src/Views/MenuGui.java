package Views;

import Logic.BilgiAction;
import Logic.CloseAction;
import Logic.GecenSureAction;
import Logic.KartlariCozAction;
import Logic.UnvanlarAction;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class MenuGui extends JMenu{
    private static MenuGui menuGui;
    OyunEkraniGui oyunekrani;
    SkorEkraniGui skorekrani;
    JMenuBar MenuBar;
    CloseAction closeAction;
    BilgiAction bilgiAction;
    GecenSureAction gecenSureAction;
    UnvanlarAction unvanlarAction;
    KartlariCozAction kartlariCozAction;
    
    private MenuGui(OyunEkraniGui oyunekrani, SkorEkraniGui skorekrani) {
        
        setOyunekrani(oyunekrani);
        setSkorekrani(skorekrani);
        
        JMenuBar MenuBar = new JMenuBar();
        setMenuBar(MenuBar);
        
        JMenu MnIslem = new JMenu(" İşlemler ");
        JMenuItem MNIYenidenDagit = new JMenuItem("Kartları Dağıt");
        MNIYenidenDagit.addActionListener(skorekrani.getDagitAction());
        MnIslem.add(MNIYenidenDagit);
        JMenuItem MNIOyunuCoz = new JMenuItem("Oyunu Çöz");
        MNIOyunuCoz.addActionListener(getKartlariCozAction());
        MnIslem.add(MNIOyunuCoz);
        JMenuItem MNIOyundanCik = new JMenuItem("Oyundan Çık");
        MnIslem.add(MNIOyundanCik);
        MNIOyundanCik.addActionListener(getCloseAction());
        
        JMenu MnDurum = new JMenu(" Durum ");
        JMenuItem MNIGecenSure = new JMenuItem("Toplam Sureniz");
        MNIGecenSure.addActionListener(getGecenSureAction());
        MnDurum.add(MNIGecenSure);
        JMenuItem MNIUnvanDurumu = new JMenuItem("Hangi Ünvanlar Var?");
        MNIUnvanDurumu.addActionListener(getUnvanlarAction());
        MnDurum.add(MNIUnvanDurumu);
        
        JMenu MnYardim = new JMenu(" Yardım ");
        JMenuItem MNINasilOynarim = new JMenuItem("Nasıl Oynarım");
        MNINasilOynarim.addActionListener(getBilgiAction());
        MnYardim.add(MNINasilOynarim);
        
        getMenuBar().add(MnIslem);
        getMenuBar().add(MnDurum);
        getMenuBar().add(MnYardim);
       
    }
    
    public static MenuGui menuGui(OyunEkraniGui oyunEkraniGui, SkorEkraniGui skorekrani){
        if (menuGui==null)
            menuGui = new MenuGui(oyunEkraniGui, skorekrani);
        return menuGui;
    }

    public OyunEkraniGui getOyunekrani() {
        return oyunekrani;
    }

    public void setOyunekrani(OyunEkraniGui oyunekrani) {
        this.oyunekrani = oyunekrani;
    }

    public SkorEkraniGui getSkorekrani() {
        return skorekrani;
    }

    public void setSkorekrani(SkorEkraniGui skorekrani) {
        this.skorekrani = skorekrani;
    }

    public JMenuBar getMenuBar() {
        return MenuBar;
    }

    public void setMenuBar(JMenuBar MenuBar) {
        this.MenuBar = MenuBar;
    }

    public CloseAction getCloseAction() {
        if (closeAction == null) {
            closeAction = new CloseAction();
        }
        return closeAction;
    }

    public void setCloseAction(CloseAction closeAction) {
        this.closeAction = closeAction;
    }

    public BilgiAction getBilgiAction() {
        if (bilgiAction == null)
            bilgiAction = new BilgiAction();
        return bilgiAction;
    }

    public void setBilgiAction(BilgiAction bilgiAction) {
        this.bilgiAction = bilgiAction;
    }

    public GecenSureAction getGecenSureAction() {
        if (gecenSureAction==null)
            gecenSureAction = new GecenSureAction();
        return gecenSureAction;
    }

    public void setGecenSureAction(GecenSureAction gecenSureAction) {
        this.gecenSureAction = gecenSureAction;
    }

    public UnvanlarAction getUnvanlarAction() {
        if (unvanlarAction==null)
            unvanlarAction = new UnvanlarAction();
        return unvanlarAction;
    }

    public KartlariCozAction getKartlariCozAction() {
        if (kartlariCozAction==null)
            kartlariCozAction = new KartlariCozAction();
        return kartlariCozAction;
    }
    
    
}
