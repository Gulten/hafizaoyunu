
package Views;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class MainGui {
    JFrame jframe;
    JPanel jpanel;
    OyunEkraniGui oyunekrani;
    SkorEkraniGui skorekrani;
    MenuGui menu;
    
    public MainGui(){
        getJframe();
        getJpanel().add(getOyunekrani());
        getJpanel().add(getSkorekrani());
        getJframe().setJMenuBar(getMenu().getMenuBar());
        jframe.setVisible(true);
    }

    public JFrame getJframe() {
         if (jframe == null){
            jframe = new JFrame("Hafıza Oyunu");
            jframe.setSize(900, 640); //boyutlari
            jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//varsayılan kapat butonu
            jframe.setResizable(false);//sabit boyut
            jframe.setLocationRelativeTo(null);//ekrani ortalayabilmesi icin
            jframe.setContentPane(getJpanel());
           
        }
         return jframe;
    }

    public void setJframe(JFrame jframe) {
        this.jframe = jframe;
    }
    
    public JPanel getJpanel() {
        if (jpanel == null){
            jpanel=new JPanel();
            jpanel.setLayout(null);
            jpanel.setBackground(Color.decode("#000"));
        }
        return jpanel;
    }

    public void setJpanel(JPanel jpanel) {
        this.jpanel = jpanel;
    }

    public OyunEkraniGui getOyunekrani() {
        if (oyunekrani == null){
            oyunekrani = new OyunEkraniGui();
        }
        return oyunekrani;
    }

    public void setOyunekrani(OyunEkraniGui oyunekrani) {
        this.oyunekrani = oyunekrani;
    }
    
    public SkorEkraniGui getSkorekrani() {
        if (skorekrani == null){
            skorekrani = new SkorEkraniGui(oyunekrani);
        }
        return skorekrani;
    }

    public void setSkorekrani(SkorEkraniGui skorekrani) {
        this.skorekrani = skorekrani;
    }

    public MenuGui getMenu() {
        if (menu == null){
            menu = MenuGui.menuGui(oyunekrani, skorekrani);
        }
        return menu;
    }

    public void setMenu(MenuGui menu) {
        this.menu = menu;
    }
    
    
}
