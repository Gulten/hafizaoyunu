
package Views;

import Logic.DagitAction;
import Logic.Kartlar;
import Logic.SeviyeAction;
import java.awt.Color;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

public class SkorEkraniGui extends JPanel {
    JButton dagitButton;
    JLabel skorLabel;
    JLabel sureLabel;
    JLabel sureKazanLabel;
    JLabel unvanLabel;
    JLabel mesajLabel;
    ButtonGroup seviyeButtonGroup;
    JRadioButton seviyeButton1;
    JRadioButton seviyeButton2;
    DagitAction dagitAction;
    SeviyeAction seviyeAction;
    OyunEkraniGui oyunekrani;
    private int skor;
    Kartlar kartlar;
    int eksure;
    
    public SkorEkraniGui(OyunEkraniGui oyunekrani){
        this.skor = 0;
        setBounds(603, 1, 295, 591);
        setBackground(Color.decode("#EBEBF9"));
        setOyunekrani(oyunekrani);
        add(getDagitButton());
        add(getSkorLabel());
        add(getSureLabel());
        add(getSureKazanLabel());
        add(getUnvanLabel());
        add(getMesajLabel());

        add(getSeviyeButton1());
        add(getSeviyeButton2());
        
        setLayout(null);
        
    }
    
    public JButton getDagitButton() {
        if (dagitButton == null) {
            dagitButton = new JButton("Başla");
            dagitButton.setForeground(Color.decode("#FFFFFF"));
            dagitButton.setBackground(Color.decode("#FF0000"));
            dagitButton.setOpaque(true);
            dagitButton.setBorderPainted(false);
            dagitButton.setVisible(true);
            dagitButton.setBounds(85, 230, 130, 30);
            dagitButton.addActionListener(getDagitAction());
        }
        return dagitButton;
    }

    public void setDagitButton(JButton dagitButton) {
        this.dagitButton = dagitButton;
    }

    public DagitAction getDagitAction() {
        if (dagitAction == null) {
            dagitAction = new DagitAction(this);
        }
        return dagitAction;
    }

    public void setDagitAction(DagitAction dagitAction) {
        this.dagitAction = dagitAction;
    }

    public SeviyeAction getSeviyeAction() {
        if (seviyeAction == null) {
            seviyeAction = new SeviyeAction(this);
        }
        return seviyeAction;
    }

    public void setSeviyeAction(SeviyeAction seviyeAction) {
        this.seviyeAction = seviyeAction;
    }
    
    

    public OyunEkraniGui getOyunekrani() {
        return oyunekrani;
    }

    public void setOyunekrani(OyunEkraniGui oyunekrani) {
        this.oyunekrani = oyunekrani;
    }

    public JLabel getSkorLabel() {
        if (skorLabel == null){
            skorLabel = new JLabel("Skorunuz : 0",SwingConstants.CENTER);
            skorLabel.setForeground(Color.decode("#ffffff"));
            skorLabel.setBackground(Color.decode("#BABAEE"));
            skorLabel.setOpaque(true);
            skorLabel.setBounds(10, 10, 275, 30);
        }
        
        return skorLabel;
    }

    public void setSkorLabel(JLabel skorLabel) {
        this.skorLabel = skorLabel;
    }
    
    public void setSkorLabelText(String text) {
        this.skorLabel.setText(text);
    }

    public int getSkor() {
        return skor;
    }

    public void setSkor(int skor) {
        this.skor = skor;
        setSkorLabelText("Skorunuz : " + skor);
    }

    public JLabel getSureLabel() {
        if (sureLabel == null){
            sureLabel = new JLabel("Kalan Süreniz : 0",SwingConstants.CENTER);
            sureLabel.setForeground(Color.decode("#ffffff"));
            sureLabel.setBackground(Color.decode("#A7A7E5"));
            sureLabel.setOpaque(true);
            sureLabel.setBorder(new LineBorder(Color.decode("#BABAEE")));
            sureLabel.setBounds(10, 50, 275, 30);
        }
        return sureLabel;
    }

    public void setSureLabel(JLabel sureLabel) {
        this.sureLabel = sureLabel;
    }
    
    public void setsureLabelText(String text) {
        this.sureLabel.setText("Kalan Süreniz : " + text);
    }

    public int getEksure() {
        return eksure;
    }

    public void setEksure(int eksure) {
        this.eksure = eksure;
        setsureKazanLabelText(String.valueOf(eksure));
    }

    public JLabel getSureKazanLabel() {
        if (sureKazanLabel == null){
            sureKazanLabel = new JLabel("Henüz Ek Süre Kazanamadınız :(",SwingConstants.CENTER);
            sureKazanLabel.setForeground(Color.decode("#ffffff"));
            sureKazanLabel.setBackground(Color.decode("#CBCBEE"));
            sureKazanLabel.setOpaque(true);
            sureKazanLabel.setBorder(new LineBorder(Color.decode("#cecece")));
            sureKazanLabel.setBounds(10, 110, 275, 30);
        }
        return sureKazanLabel;
    }

    public void setSureKazanLabel(JLabel sureKazanLabel) {
        this.sureKazanLabel = sureKazanLabel;
    }

    public void setsureKazanLabelText(String text){
        if (Integer.parseInt(text)==0)
            this.sureKazanLabel.setText("Henüz Ek Süre Kazanamadınız :(");
        else
            this.sureKazanLabel.setText("Kazandığın Ek Süre : + " + text);
    }
    
    public JLabel getUnvanLabel() {
        if (unvanLabel == null){
            unvanLabel = new JLabel("Ünvan",SwingConstants.CENTER);
            unvanLabel.setForeground(Color.decode("#8A8ADA"));
            unvanLabel.setBackground(Color.decode("#EBEBF9"));
            unvanLabel.setOpaque(true);
            unvanLabel.setBorder(new LineBorder(Color.decode("#BABAEE")));
            unvanLabel.setBounds(10, 330, 275, 30);
        }
        return unvanLabel;
    }

    public void setUnvanLabel(JLabel unvanLabel) {
        this.unvanLabel = unvanLabel;
    }
    
    public void setunvanLabelText(String text) {
        this.unvanLabel.setText(text);
    }

    public JLabel getMesajLabel() {
        if (mesajLabel == null){
            mesajLabel = new JLabel("Haydi Bir Ünvan Kazan :)",SwingConstants.CENTER);
            mesajLabel.setForeground(Color.decode("#8A8ADA"));
            mesajLabel.setBackground(Color.decode("#EBEBF9"));
            mesajLabel.setOpaque(true);
            mesajLabel.setBorder(new LineBorder(Color.decode("#BABAEE")));
            mesajLabel.setBounds(10, 370, 275, 70);
        }
        return mesajLabel;
    }

    public void setMesajLabel(JLabel mesajLabel) {
        this.mesajLabel = mesajLabel;
    }

    public void setmesajLabelText(String text) {
        this.mesajLabel.setText(text);
    }
    
    public ButtonGroup getSeviyeButtonGroup() {
        if (seviyeButtonGroup == null){
            seviyeButtonGroup = new ButtonGroup();      
        }
        return seviyeButtonGroup;
    }

    public void setSeviyeButtonGroup(ButtonGroup seviyeButtonGroup) {
        this.seviyeButtonGroup = seviyeButtonGroup;
    }

    public JRadioButton getSeviyeButton1() {
        if (seviyeButton1 == null){
            seviyeButton1 = new JRadioButton("Acemi");
            seviyeButton1.setBounds(40, 155, 100, 30);
            seviyeButton1.setSelected(true);
            seviyeButton1.addActionListener(getSeviyeAction());
            getSeviyeButtonGroup().add(getSeviyeButton1());
        }
        return seviyeButton1;
    }

    public void setSeviyeButton1(JRadioButton seviyeButton1) {
        this.seviyeButton1 = seviyeButton1;
    }

    public JRadioButton getSeviyeButton2() {
        if (seviyeButton2 == null){
            seviyeButton2 = new JRadioButton("Uzman");
            seviyeButton2.setBounds(160, 155, 100, 30);
            seviyeButton2.addActionListener(getSeviyeAction());
            getSeviyeButtonGroup().add(getSeviyeButton2());
        }
        return seviyeButton2;
    }

    public void setSeviyeButton2(JRadioButton seviyeButton2) {
        this.seviyeButton2 = seviyeButton2;
    }
    
        
}
