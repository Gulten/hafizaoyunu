
package Logic;

import Logic.KartAction;
import Views.OyunEkraniGui;
import Views.SkorEkraniGui;
import java.awt.Color;
import java.util.*;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class Kartlar {
    final static int toplam_sure = 80;
    private static Kartlar kartlar;
    Seviyeler seviyeler;
    Sure sure;
    private int kart_sayisi;
    private JButton[] tum_kartlar;
    private int acik_kart;
    private KartAction[] kartaction;
    private int kart_genislik, kart_yukseklik;
    private OyunEkraniGui oyunekrani;
    private SkorEkraniGui skorekrani;
    private ArrayList<Integer> resim_dizisi;
    private int resim_sayisi;
    private int acilan_kart_sayisi;
    private int kalan_kart_sayisi;
    private boolean kartlar_ekranda;
    private int oyun_suresi;
    
    private Kartlar(){
       seviyeler = Seviyeler.seviyeler();
       setOyun_suresi(toplam_sure);
       sure = Sure.sure(this); 
       sure.setGecen_sure(getOyun_suresi());
       
       acik_kart = 0;
       acilan_kart_sayisi = 0;
       
       kartlar_ekranda=false;
    }
    
    public static Kartlar kartlar(){
        if (kartlar==null)
            kartlar = new Kartlar();
            
        return kartlar;
    }

    public int getKart_sayisi() {
        return kart_sayisi;
    }

    public void setKart_sayisi() {
        this.kart_sayisi =  seviyeler.getSutun() * seviyeler.getSatir();
        
        kart_genislik = (OyunEkraniGui.ekran_genisligi / seviyeler.getSutun())-5;
        setKart_genislik(kart_genislik);
        
        kart_yukseklik = (OyunEkraniGui.ekran_genisligi / seviyeler.getSatir())-5;
        setKart_yukseklik(kart_yukseklik);
       
    }

    public int getAcilan_kart_sayisi() {
        return acilan_kart_sayisi;
    }

    public void addAcilan_kart_sayisi() {
        this.acilan_kart_sayisi = this.acilan_kart_sayisi + 2;
    }

    public int getKalan_kart_sayisi() {
        return getKart_sayisi() - getAcilan_kart_sayisi();
    }

    public int getResim_sayisi() {
        return resim_sayisi;
    }

    public void setResim_sayisi(int resim_sayisi) {
        this.resim_sayisi = resim_sayisi;
    }
    
    public int getKart_genislik() {
        return kart_genislik;
    }

    public void setKart_genislik(int kart_genislik) {
        this.kart_genislik = kart_genislik;
    }

    public int getKart_yukseklik() {
        return kart_yukseklik;
    }

    public void setKart_yukseklik(int kart_yukseklik) {
        this.kart_yukseklik = kart_yukseklik;
    }

    public void setOyunekrani(OyunEkraniGui oyunekrani) {
        this.oyunekrani = oyunekrani;
    }

    public OyunEkraniGui getOyunekrani() {
        return oyunekrani;
    }

    public SkorEkraniGui getSkorekrani() {
        return skorekrani;
    }

    public void setSkorekrani(SkorEkraniGui skorekrani) {
        this.skorekrani = skorekrani;
    }

    public JButton[] getTum_kartlar() {
        return tum_kartlar;
    }

    public void setTum_kartlar(JButton[] tum_kartlar) {
        this.tum_kartlar = tum_kartlar;
    }
    
    public void setTum_kartlar(int index, JButton kart) {
        this.tum_kartlar[index] = kart;
    }
    
    public JButton getTum_kartlar(int index) {
        return tum_kartlar[index];
    }

    public int getAcik_kart() {
        return acik_kart;
    }

    public void setAcik_kart(int acik_kart) {
        this.acik_kart = acik_kart;
    }
    
    public void sifirla(){
       sure.setGecen_sure(getOyun_suresi());
       
       acik_kart = 0;
       acilan_kart_sayisi = 0;
    }
    
    public void KartlariDagit(OyunEkraniGui oyunekrani, SkorEkraniGui skorekrani, int seviye){
        int sol = 0, sag = 0, index = 0;
        
        if (kartlar_ekranda) {
            KartlariTemizle();
            skorekrani.setSkor(0);
            skorekrani.setEksure(0);
        }
        else{
            this.kartlar_ekranda=true;
        }
        
        seviyeler.setSeviye(seviye);
        sure.Zamanlayici(true);
        setKart_sayisi();
        int genislik = getKart_genislik();
        int yukseklik = getKart_yukseklik();
        
        setOyunekrani(oyunekrani);
        setSkorekrani(skorekrani);
        
        tum_kartlar = new JButton[getKart_sayisi()];
        setTum_kartlar(tum_kartlar);
        kartaction = new KartAction[getKart_sayisi()];
        for (int i = 0;i < seviyeler.getSutun();i++){
            
            for (int j =0;j < seviyeler.getSatir();j++){
   
                tum_kartlar[index] = new JButton();
                getOyunekrani().add(tum_kartlar[index]);
                tum_kartlar[index].setBackground(Color.white);
                tum_kartlar[index].setBackground(Color.decode("#FFFFFF"));
                tum_kartlar[index].setOpaque(true);
                tum_kartlar[index].setBorder(new LineBorder(Color.decode("#ac3973"),1));
                tum_kartlar[index].setBounds(sol, sag, genislik, yukseklik);
                
                kartaction[index] = new KartAction(index);
                tum_kartlar[index].addActionListener(kartaction[index]);
                
                index++;
                sol = sol + genislik;
            }
            
            sol = 0;
            sag = sag + yukseklik;
        }
        
        setTum_kartlar(tum_kartlar);
        
        resim_dizisi = new ArrayList<Integer>(getKart_sayisi());//resim dizisi oluşturuluyor
        
        for (int i = 1; i <= 2; i++){
            for (int j = 1; j <= getKart_sayisi()/2; j++){
                //resim dizisi atanıyor - random
                resim_dizisi.add(j);
            }
        }    
        Collections.shuffle(resim_dizisi, new Random());
        setResim_dizisi(resim_dizisi);
        
        for (Iterator<Integer> it = resim_dizisi.iterator(); it.hasNext();) {
            Integer number = it.next();
        }
    }

    public void KartlariTemizle(){
     getOyunekrani().removeAll();
     getOyunekrani().revalidate();
     getOyunekrani().repaint();
    }
    
    public ArrayList<Integer> getResim_dizisi() {
        return resim_dizisi;
    }

    public void setResim_dizisi(ArrayList<Integer> resim_dizisi) {
        this.resim_dizisi = resim_dizisi;
    }

    public int getOyun_suresi() {
        return oyun_suresi;
    }

    public void setOyun_suresi(int oyun_suresi) {
        this.oyun_suresi = oyun_suresi;
    }
    
    
}
