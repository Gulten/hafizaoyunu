
package Logic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class KartlariCozAction implements ActionListener{
   Kartlar k;
    
    public KartlariCozAction(){
        this.k = Kartlar.kartlar();
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (k.getKalan_kart_sayisi()==0){
            JOptionPane.showMessageDialog(null, "Ekranda Hiç Kart Yok");
        }
        else{
            for (int i=0;i<k.getKart_sayisi();i++){

                Integer resim_no = k.getResim_dizisi().get(i);
                Icon kartImage = new ImageIcon(getClass().getResource("../Views/" + resim_no + ".png"));
                k.getTum_kartlar(i).setIcon(kartImage);
                k.sure.Zamanlayici(false);
                k.getOyunekrani().setEnabled(false);
            }
        }
    }
    
}
