
package Logic;

import Views.OyunEkraniGui;
import Views.SkorEkraniGui;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.util.*;
import javax.swing.JOptionPane;

public class KartAction implements ActionListener{
    Kartlar k;
    int index;
    int kart_no;
    boolean es_kartlar;
    ArrayList<Integer> resim_dizisi;
    int resim_no;
    Sure sure;
    
    public KartAction(int index) {
        this.k = Kartlar.kartlar();
        this.sure = Sure.sure(k);
        this.index = index;
        this.kart_no = index + 1;
        this.es_kartlar = false;
        
        this.resim_dizisi = new ArrayList<Integer>(k.getKart_sayisi());
        this.resim_dizisi = k.getResim_dizisi();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int index_acilan_kart = 0;
        //kapalı bir karta tıklanmışsa açılış
       if (k.getTum_kartlar(index).getIcon() == null){
            resim_no = k.getResim_dizisi().get(index);
            Icon kartImage = new ImageIcon(getClass().getResource("../Views/" + resim_no + ".png"));
            k.getTum_kartlar(index).setIcon(kartImage);
            
            if (k.getAcik_kart()==0) {
                k.setAcik_kart(kart_no);
            }
            else {
                
                //kartlar aynı ise skor tabelasına ekle setSkor veya add kodu eklenecek
                //..
                
                if (k.getTum_kartlar(k.getAcik_kart()-1).getIcon().toString().equals(k.getTum_kartlar(index).getIcon().toString())){
                    es_kartlar = true;
                    //skor ekranına puan ekle
                    k.getSkorekrani().setSkor(k.getSkorekrani().getSkor() + 100);
                    k.addAcilan_kart_sayisi();
                     
                    if (k.getKalan_kart_sayisi()==0){
                         k.sure.setKontrol(true);
                    }
                    
                    if( ( k.getAcilan_kart_sayisi()%4) == 0 ){
                        sure.setGecen_sure(sure.getGecen_sure() + 5);
                        k.getSkorekrani().setEksure(k.getSkorekrani().getEksure()+ 5);
                    }
                    
                    
                        
                }
                
                Timer my_timer = new Timer();
                TimerTask beklet = new TimerTask() {
                    @Override
                    public void run() {
                        
                        k.getTum_kartlar(k.getAcik_kart()-1).setIcon(null);
                        k.getTum_kartlar(index).setIcon(null);
                                            
                        if (es_kartlar == true) {
                            es_kartlar = false;
                            
                            //açılan kartları gizle
                            //k.getTum_kartlar(k.getAcik_kart()-1).setVisible(false); 
                            //k.getTum_kartlar(index).setVisible(false);
                            
                            //açılan kartları sil
                            k.getOyunekrani().remove(k.getTum_kartlar(k.getAcik_kart()-1));
                            k.getOyunekrani().remove(k.getTum_kartlar(index));
                            k.getOyunekrani().revalidate();
                            k.getOyunekrani().repaint();
                            
                            
                        }
                        
                        k.setAcik_kart(0);
                        
                        my_timer.cancel();
                       
                    }
                };
                
                my_timer.schedule(beklet, 200);//kartların eşleşmesini kullanıcı görmeli, delay
            }
       }//kapalı bir karta tıklanmışsa kapanış
    }
    
}