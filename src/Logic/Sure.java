package Logic;

import Views.SkorEkraniGui;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JOptionPane;

public class Sure extends Timer{
    private static Sure sure;
    private int gecen_sure;
    private boolean durum;
    Kartlar kartlar;
    private boolean kontrol;
    private String unvan;
    private String mesaj;
    Timer zamanla;
    TimerTask sureyi_ilerlet;
    
    private Sure(Kartlar kartlar){
        this.kartlar = kartlar;
    }
    
    public static Sure sure(Kartlar kartlar){
        if (sure==null)
            sure = new Sure(kartlar);
        return sure;
    }

    public int getGecen_sure() {
        return gecen_sure;
    }

    public void setGecen_sure(int gecen_sure) {
        this.gecen_sure = gecen_sure;
    }

    public boolean isDurum() {
        return durum;
    }

    public void setDurum(boolean durum) {
        this.durum = durum;
    }

    public boolean isKontrol() {
        return kontrol;
    }

    public void setKontrol(boolean kontrol) {
        this.kontrol = kontrol;
    }
    
    public void Zamanlayici(boolean durum){
        setDurum(durum);
        setKontrol(false);
        
        zamanla = new Timer();
                
        if (sureyi_ilerlet != null)
        sureyi_ilerlet.cancel();
         
        sureyi_ilerlet = new TimerTask() {
            @Override
            public void run() {
                setGecen_sure(getGecen_sure()-1);
                kartlar.getSkorekrani().setsureLabelText(String.valueOf(getGecen_sure()));
                
               if (getGecen_sure()<=0){//süre kalmadıysa zamanlayıcıyı durdur, oyunu bitir
                   sureyi_ilerlet.cancel();
                   kartlar.getSkorekrani().setmesajLabelText(getUnvan());
                   JOptionPane.showMessageDialog(null, getMesaj());
                   kartlar.KartlariTemizle();
               }
               else if (isKontrol()){
                   sureyi_ilerlet.cancel();
                   kartlar.getSkorekrani().setSkor(kartlar.getSkorekrani().getSkor() + (getGecen_sure()*10));
                   kartlar.getSkorekrani().setmesajLabelText(getUnvan());
                   JOptionPane.showMessageDialog(null, getMesaj());
                   kartlar.KartlariTemizle();
               }
            }
        };
                
        if (isDurum())
            zamanla.scheduleAtFixedRate(sureyi_ilerlet, 1, 1000);//zamanlayıcı bir azaltma görevi
        else
            sureyi_ilerlet.cancel();
    }

    public String getUnvan() {
        
        if (getGecen_sure()>(kartlar.getOyun_suresi())/2 ){
            this.unvan = "**Kartların Efendisi**";
            this.mesaj = "<html><center>Gelmiş geçmiş tüm kart<br>ustaları önünde saygıyla eğiliyor.<br>Herkes seni kıskanıyor :D</center></html>";
        } 
        else if(getGecen_sure()>(kartlar.getOyun_suresi())/3){
            this.unvan = "Mega Hafıza B-)";
            this.mesaj = "<html><center>Tüm gözler üstünde!<br> Düşman çatlatıyorsun :)</center></html>";
        }
        else if(getGecen_sure()>(kartlar.getOyun_suresi())/4){
            this.unvan = "Gönüllerin Efendisi :-D";
            this.mesaj = "<html><center>Varsın onlar kral olsun,<br> sen gönüllerin efendisisin :P</center></html>";
        }
        else if(getGecen_sure()>(kartlar.getOyun_suresi())/5){
            this.unvan = "Kayıp Balık Nemo :-|";
            this.mesaj = "Biraz daha omega-3 lütfen :|";
        }
        else{
            this.unvan = "Olmasa da Olur :-)";
            this.mesaj = "Çıkışlar soldan, bari bunu unutma :(";
        }
        
        return unvan;
    }

    public String getMesaj() {
        return mesaj;
    }

}
