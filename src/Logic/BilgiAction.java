
package Logic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class BilgiAction implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
        JOptionPane.showMessageDialog(null, "Dağıt butonuna tıkla ve bir ünvan kazan! Süren sınırlı, dikkat et! Her iki kart eşleştirmende 5 sn kazanacaksın, elini çabuk tut :)");
    }
    
}
