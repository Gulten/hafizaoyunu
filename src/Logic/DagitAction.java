
package Logic;

import Views.OyunEkraniGui;
import Views.SkorEkraniGui;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DagitAction implements ActionListener{
    OyunEkraniGui oyunekrani;
    SkorEkraniGui skorekrani;
    Kartlar kartlar;
    
    public DagitAction(SkorEkraniGui skorekrani) {
        this.skorekrani = skorekrani;
        this.oyunekrani = skorekrani.getOyunekrani();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (kartlar==null)
            kartlar = Kartlar.kartlar();
        else
            kartlar.sifirla();
        
        if (this.skorekrani.getSeviyeButton2().isSelected())
            kartlar.seviyeler.setSeviye(2);
        else
            kartlar.seviyeler.setSeviye(1);
            
        kartlar.KartlariDagit(this.oyunekrani,this.skorekrani,kartlar.seviyeler.getSeviye());
        //süre burada başlatılacak
    }
    
}
