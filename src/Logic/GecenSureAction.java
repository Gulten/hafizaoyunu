
package Logic;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class GecenSureAction implements ActionListener{
    Sure sure;
    
    public GecenSureAction(){
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        JOptionPane.showMessageDialog(null, "Toplam süreniz " + Kartlar.toplam_sure + " saniyedir");        
    }
    
}
