
package Logic;

public class Seviyeler {
    private static Seviyeler seviyeler;
    private int seviye = 1, satir, sutun;
    
    private Seviyeler(){
        
    }
    
    public static Seviyeler seviyeler(){
       if (seviyeler==null)
           seviyeler = new Seviyeler();
       return seviyeler;
    }

    public int getSeviye() {
        return seviye;
    }

    public void setSeviye(int seviye) {
        this.seviye = seviye;
        if ( seviye == 1  ){
            setSutun(4);
            setSatir(4);
        }
        else if ( seviye == 2 ) {
            setSutun(6);
            setSatir(6);
        }
    }
    
    public int getSutun() {
        return sutun;
    }

    public void setSutun(int sutun) {  
        this.sutun = sutun;
    }

    public int getSatir() {
        return satir;
    }

    public void setSatir(int satir) {
        this.satir = satir;
    }
    
    
    
}
